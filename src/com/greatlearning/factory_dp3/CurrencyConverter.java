package com.greatlearning.factory_dp3;

public interface CurrencyConverter {

	  double convertToINR(double amount) ;
}

package com.greatlearning.factory_dp3;

public class CurrencyFactory {
		   public static double convertCurrencyToINR (String currency, double amount) {
		      if (currency. equalsIgnoreCase ("USD")){
		         return new GBPConverter().convertToINR(amount);
		      }else if(currency. equalsIgnoreCase ("GBP")){
			     return new DollarConvertor().convertToINR(amount);
		      }else{   
		         throw new IllegalArgumentException("No such currency");
		      }
		   }

}

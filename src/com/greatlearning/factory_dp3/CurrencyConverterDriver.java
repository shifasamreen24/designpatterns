package com.greatlearning.factory_dp3;

import java.util.Scanner;

public class CurrencyConverterDriver {

	public static void main(String args[]) {
		
    	System.out.println("\n");
    	System.out.println("Enter currency type you want to convert - USD or GBP");
		Scanner scanner = new Scanner(System.in);
    	String option = scanner.next();
    	System.out.println("Enter the amount");
    	double amountInput = scanner.nextDouble();

    	double amountFinal = CurrencyFactory.convertCurrencyToINR(option, amountInput);
	}
}

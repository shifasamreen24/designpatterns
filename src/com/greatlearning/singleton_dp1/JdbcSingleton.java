package com.greatlearning.singleton_dp1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcSingleton {
	
	  private static volatile JdbcSingleton instance;  
	  private Connection connection;
	  private String url = "jdbc:mysql://localhost:3306/userdata";
	  private String username = "root";
	  private String password = "@RootpassworD";
	  
	  private JdbcSingleton() throws SQLException {
	    try {
	      Class.forName("org.mysql.Driver");
	      this.connection = DriverManager.getConnection(url, username, password);
	    } catch (ClassNotFoundException ex) {
	      System.out.println("Something is wrong with the DB connection String : " + ex.getMessage());
	    }
	  }
	  
	  public Connection getConnection() {
	    return connection;
	  }
	 
	  public static JdbcSingleton getInstance() throws SQLException {
	    if (instance == null) {    	
		    synchronized(JdbcSingleton.class) {    	
		    	if (instance == null) 
		    		instance = new JdbcSingleton();
		    }   
	    } 
	    else if (instance.getConnection().isClosed()) {
	      instance = new JdbcSingleton();
	    }
	    return instance;
	  }

	  // User Method- 1
	  protected void method1() {
		  System.out.println("Method 1 executed");
	  }
	  // User Method- 2
	  protected void method2() {
		  System.out.println("Method 2 executed");
	  }

}

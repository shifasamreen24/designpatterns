package com.greatlearning.singleton_dp1;

import java.sql.SQLException;

public class JdbcDriver {

	public static void main(String[] args) throws SQLException {  
		
		JdbcSingleton jdbcInstance= JdbcSingleton.getInstance();  
		jdbcInstance.method1();
		jdbcInstance.method2();
		
	}
}

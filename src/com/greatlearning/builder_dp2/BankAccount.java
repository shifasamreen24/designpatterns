package com.greatlearning.builder_dp2;

import java.util.ArrayList;

public class BankAccount {

	// attributes
	private final long accountNumber;
	private final String accountType;
	private final String branch;
	private final double balance;
	private final ArrayList<Integer> atmTransactions;
	private final String emiSchedule;
	
    //private constructor
    private BankAccount( Builder builder ){
    	 this.accountNumber=builder.accountNumber;
    	 this.accountType=builder.accountType;
    	 this.branch=builder.branch;
    	 this.balance=builder.balance;
    	 this.atmTransactions=builder.atmTransactions;
    	 this.emiSchedule=builder.emiSchedule;
    }


	//getter and setters
	public long getAccountNumber() {
		return accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public String getBranch() {
		return branch;
	}

	public double getBalance() {
		return balance;
	}

	public ArrayList<Integer> getAtmTransactions() {
		return atmTransactions;
	}

	public String getEmiSchedule() {
		return emiSchedule;
	}

	//toString
    @Override
	public String toString() {
		return "Builder [accountNumber=" + accountNumber + ", accountType=" + accountType + ", branch=" + branch
				+ ", balance=" + balance + ", atmTransactions=" + atmTransactions + ", emiSchedule=" + emiSchedule
				+ "]";
	}
	
	// Builder class

	public static class Builder {

		private  long accountNumber; // required
	    private  String accountType; // required
	    private  String branch; // required
	    private  double balance; // required
	    private  ArrayList<Integer> atmTransactions; // optional
	    private  String emiSchedule; // optional
	    
	    
	    public Builder(long accountNumber) {
	        this.accountNumber = accountNumber;
	    }
	
	    public Builder withAccountType(String accountType){
	        this.accountType = accountType;
	        return this;  //By returning the builder each time, we can create a fluent interface.
	    }
	
	    public Builder atBranch(String branch){
	        this.branch = branch;
	        return this;
	    }
	
	    public Builder openingBalance(double balance){
	        this.balance = balance;
	        return this;
	    }
	
	    public Builder addAtmTransaction(ArrayList atmTransactions){
	        this.atmTransactions = atmTransactions;
	        return this;
	    }
	 
	    public Builder emiSchedule(String emiSchedule){
        this.emiSchedule = emiSchedule;
        return this;
    }
	
	    //builder function
	    public BankAccount build(){
            return new BankAccount(this);
        }
	}

	
}
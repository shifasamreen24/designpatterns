package com.greatlearning.builder_dp2;

import java.sql.SQLException;
import java.util.ArrayList;

public class CreateBankAccountDriver {

	 public static void main( String[] args ) throws SQLException
	    {
		BankAccount account1 = new BankAccount.Builder(187832486)
	            .withAccountType("SAVING")
	            .atBranch("Herman Melville")
	            .openingBalance(2000.0)
	            .addAtmTransaction(  new ArrayList<Integer>() )
	            .emiSchedule("Tuesday")
	            .build();
		
		System.out.println(account1.toString());
	    }
	 
}
